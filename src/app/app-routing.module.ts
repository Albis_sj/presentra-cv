import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendaComponent } from './component/agenda/agenda.component';
import { ContactoComponent } from './component/contacto/contacto.component';
import { EstudiosComponent } from './component/estudios/estudios.component';
import { InicioComponent } from './component/inicio/inicio.component';
import { PortafolioComponent } from './component/portafolio/portafolio.component';
import { SobreMiComponent } from './component/sobre-mi/sobre-mi.component';

const routes: Routes = [
  {path: 'sobre-mi', component: SobreMiComponent},
  {path: 'inicio', component: InicioComponent},
  {path: 'estudios', component: EstudiosComponent},
  {path: 'portafolio', component: PortafolioComponent},
  
  {path: 'agenda', component: AgendaComponent},
  // {path: 'editar-cita/:id', component: },
  // {path: 'ver-cita/:id', component: },
  {path: 'contacto', component: ContactoComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'main'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
