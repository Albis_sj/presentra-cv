import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  loading: boolean = false
  form!: FormGroup;
  mensaje!: string;
  agradecimiento: string = 'Su mensaje fue enviado con éxito';

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  crearFormulario(): void{
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      // telefono: ['', [Validators.required, Validators.minLength(8)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]]
    });
  }

  //GET
  get nombreNoValido(){
    return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched;
  }

  get correoNoValido(){
    return this.form.get('correo')?.invalid && this.form.get('correo')?.touched
  }

  get comentarioNoValido(){
    return this.form.get('comentario')?.invalid && this.form.get('comentario')?.touched
  }

  // get telefonoNoValido(){
  //   return this.form.get('telefono')?.invalid && this.form.get('telefono')?.touched
  // }

  ngOnInit(): void {
  }

  guardar(): void{
    console.log(this.form);
    this.mensaje = this.agradecimiento
    this.LimpiarFormulario()
    setTimeout(() => {
      this.mensaje = ''
    }, 3000);

  }

  LimpiarFormulario(){
    this.form.reset();
  }


  MensajeEnviado(){
    this.mensaje = this.agradecimiento
  }

}
