import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PortafolioComponent } from './component/portafolio/portafolio.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { InicioComponent } from './component/inicio/inicio.component';
import { FooterComponent } from './component/footer/footer.component';
import { EstudiosComponent } from './component/estudios/estudios.component';
import { SobreMiComponent } from './component/sobre-mi/sobre-mi.component';
import { ContactoComponent } from './component/contacto/contacto.component';
import { AgendaComponent } from './component/agenda/agenda.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PortafolioComponent,
    NavbarComponent,
    InicioComponent,
    FooterComponent,
    EstudiosComponent,
    SobreMiComponent,
    ContactoComponent,
    AgendaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
