import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CitasService {

  listaCitas!: citaI[];

  constructor() { }

  agregarTabla(cita: citaI){
    
    if(localStorage.getItem('Citas') === null){
      this.listaCitas = [];
      this.listaCitas.unshift(cita);
      localStorage.setItem('Citas', JSON.stringify(this.listaCitas))
    } else {
      this.listaCitas = JSON.parse(localStorage.getItem('Citas') || '');
      this.listaCitas.unshift(cita);
      localStorage.setItem('Citas', JSON.stringify(this.listaCitas))
    }
    console.log(this.listaCitas, 'lista de agregarTablas');
  }

  agregarCita(cita: citaI) {
    this.listaCitas.unshift(cita);
    console.log(this.listaCitas);
  }

  buscarCita(id: string): citaI{
    return this.listaCitas.find(e => e.nombre=== id) || {} as citaI
  }


  obtenerLocalStorage(){
    let citaLista = JSON.parse(localStorage.getItem("Citas") || '');
    console.log(citaLista);
    
    this.listaCitas = citaLista
    console.log(citaLista, 'obtener LocalStorage');
    return citaLista
  }



}


interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}